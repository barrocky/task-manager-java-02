package ru.parmeev.tm.constants;

public interface TerminalConstants {
    String CMD_HELP = "help";
    String CMD_VERSION = "version";
    String CMD_ABOUT = "about";
}
