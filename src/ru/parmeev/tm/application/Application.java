package ru.parmeev.tm.application;

import ru.parmeev.tm.constants.TerminalConstants;

public class Application implements TerminalConstants {

    public static void main(final String[] args) {
        displayWelcome();
        run(args);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String arg = args[0];
        if (CMD_HELP.equals(arg)) displayHelp();
        if (CMD_VERSION.equals(arg)) displayVersion();
        if (CMD_ABOUT.equals(arg)) displayAbout();
    }

    private static void displayHelp() {
        System.out.println("version - Display program version");
        System.out.println("about - Display developer info");
        System.out.println("help - Display list of terminal commands");
        System.exit(0);
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("Artem Parmeev");
        System.out.println("workandplay64@mail.ru");
        System.exit(0);
    }

}
